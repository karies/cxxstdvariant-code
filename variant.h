#include <functional>
#include <tuple>

namespace std {
  // My libc++ doesn't have it yet...
  template<int I, class... Types> using tuple_element_t
    = typename tuple_element<I, Types...>::type;
namespace experimental {
inline namespace fundamentals_vXXXX {
  namespace internals {
    typedef void (*Destructor_t)(void*);
    template<typename T>
    struct Destructor {
      static void destruct(void * t) { ((T*)(t))->~T();}
    };

    template<typename T>
    inline constexpr T vmax(T a, T b) { return (a>b) ? a : b;}


    // Get the max size of the template parameter types.
    template<class Head, typename ... Tail>
    struct maxSize {
      enum { value = vmax(sizeof(Head), (long unsigned int)(maxSize<Tail...>::value)) };
    };

    template<class T>
    struct maxSize<T>  {
      enum  { value = sizeof(T)};
    };

    // Do a placement new at buf of the type at IDX with arguments args.
    // Return the corresponding Destructor object.
    template <int IDX, class Head, class... Tail>
    struct store {
      template<class...Args>
      Destructor_t operator()(void* buf, Args...args) {
        return store<IDX - 1, Tail...>()(buf, args...);
      }
    };
    template <class Head, class... Tail>
    struct store<0, Head, Tail...> {
      template<class...Args>
      Destructor_t operator()(void* buf, Args...args) {
        typedef Head Head_t;
        new (buf)Head_t(args...);
        return &Destructor<Head_t>::destruct;
      }
    };


    // Get the type at position N
    template<size_t N, class Head, class... Tail>
    struct NthType {
      typedef typename NthType<N - 1, Tail...>::type type;
    };
    template<class Head, class... Tail>
    struct NthType<0, Head, Tail...> {
      typedef Head type;
    };


    template<size_t Idx, class Find, class Head, class... Tail>
    struct TypeIdxImpl {
      static constexpr size_t val
        = is_same<Find, Head>::value ?
          Idx : TypeIdxImpl<Idx + 1, Find, Tail...>::val;
    };
    template<size_t Idx, class Find, class Head>
    struct TypeIdxImpl<Idx, Find, Head> {
      static constexpr size_t val = is_same<Find, Head>::value ? Idx : -1;
    };

    // Get the type at position N in Types.
    template<class Find, class... Types>
    struct TypeIdx {
      static constexpr size_t val = TypeIdxImpl<0, Find, Types...>::val;
    };


    // Copy or move an object into mem; return the corresponding Destructor.
    template <class V>
    struct copyElement {
      typedef void (*Func_t)(char* mem, const V& rhs_);
      template <typename T> struct Impl {
        static Destructor_t doCopy(char* mem, const V& rhs_) {
          new (mem) T(*(T*)rhs_.data());
          return &Destructor<T>::destruct;
        }
        static Destructor_t doMove(char* mem, const V&& rhs_) {
          new (mem) T(move(*(T*)rhs_.data()));
          return &Destructor<T>::destruct;
        }
      };
    };


    // RelOp of T to U, where T is a type available through variant<V>
    // and U is available through variant<W>.
    template <class V, class W>
    struct relOpElement {
      typedef bool (*Func_t)(const V&, const W&);
      template <class T> struct ImplOne {
        template <class U> struct ImplTwo {
          static bool doEqual(const V& lhs, const W& rhs) {
            return (*(T*)rhs.data()) == (*(U*)lhs.data());
          }
          static bool doLess(const V& lhs, const W& rhs) {
            return (*(T*)rhs.data()) < (*(U*)lhs.data());
          }
          static bool doGreater(const V& lhs, const W& rhs) {
            return (*(T*)rhs.data()) > (*(U*)lhs.data());
          }
        };
      };
    };


    // Swap content of two variants.
    template <typename V>
    struct swapElement {
      typedef void (*Func_t)(V&, V&);
      template <class T> struct Impl {
        static void doSwap(V& x, V& y) {
          if (x.empty()) {
            T z{std::move(get<T>(x))};
            x = std::move(get<T>(y));
            y = std::move(z);
          }
        }
      };
    };
  } // internals


  static constexpr const size_t tuple_not_found = (size_t) -1;
  template <class T, class U> class tuple_find;  // undefined
  template <class T, class U> class tuple_find<T, const U>;
  template <class T, class U> class tuple_find<T, volatile U>;
  template <class T, class U> class tuple_find<T, const volatile U>;
  template <class T, class... Types>
  class tuple_find<T, tuple<Types...>> {
  public:
    typedef typename internals::TypeIdx<T, Types...> Idx_t;
    constexpr static size_t value = Idx_t::val;
  };



  template <class... Types>
  class variant;

  // 2.?, In-place construction
  template <class T> struct in_place_t{};
  template <class T> constexpr in_place_t<T> in_place{};

  // 2.?, class bad_variant_access
  class bad_variant_access: public exception {
  };

  // 2.?, handling of alternatives
  template <class T, class... Types>
    constexpr bool is_alternative(const variant<Types...>&) noexcept {
      typedef typename internals::TypeIdx<T, Types...> Idx_t;
      return Idx_t::val != -1;
    }

  // 2.?, value access
  template <class T, class... Types>
  bool holds_alternative(const variant<Types...>& v) noexcept {
    return !v.empty()
       && v.index() == tuple_find<T, tuple<Types...>>::value;
  }

  template <size_t I, class... Types>
    tuple_element_t<I, tuple<Types...>>&
      get(variant<Types...>& v) {
        if (v.index() != I) throw bad_variant_access();
        return *(tuple_element_t<I, tuple<Types...>>*)(v.data());
  }

  template <size_t I, class... Types>
    tuple_element_t<I, tuple<Types...>>&&
      get(variant<Types...>&& v) {
    if (v.index() != I) throw bad_variant_access();
    return *(tuple_element_t<I, tuple<Types...>>*)(v.data());
  }

  template <size_t I, class... Types>
    const tuple_element_t<I, tuple<Types...>>&
      get(const variant<Types...>& v) {
        if (v.index() != I) throw bad_variant_access();
        return *(const tuple_element_t<I, tuple<Types...>>*)(v.data());
  }

  template <class T, class... Types>
  T& get(variant<Types...>& v) {
    if (!holds_alternative<T>(v)) throw bad_variant_access();
    return *(T*)(v.data());
  }
  template <class T, class... Types>
  T&& get(variant<Types...>&& v) {
    if (!holds_alternative<T>(v)) throw bad_variant_access();
    return *(T*)(v.data());
  }
  template <class T, class... Types>
  const T& get(const variant<Types...>& v) {
    if (!holds_alternative<T>(v)) throw bad_variant_access();
    return *(const T*)(v.data());
  }

  // 2.?, relational operators
  template <class... TTypes, class... UTypes>
  bool operator==(const variant<TTypes...>& lhs,
                  const variant<UTypes...>& rhs) {
    using relOpEl_t = internals::relOpElement<variant<TTypes...>, variant<UTypes...>>;
    typedef typename relOpEl_t::Func_t CompFunc_t;
     static const CompFunc_t comp[sizeof...(TTypes)][sizeof...(UTypes)]
        = { (&relOpEl_t::template ImplOne<TTypes>::template ImplTwo<UTypes>::doEqual)... };
     if (lhs.empty() && rhs.empty()) return true;
     if (lhs.empty() != rhs.empty()) return false;
     return comp[lhs.index()][rhs.index()](lhs, rhs);
  }

  template <class... TTypes, class... UTypes>
  bool operator!=(const variant<TTypes...>& lhs,
                  const variant<UTypes...>& rhs) {
    return !(lhs == rhs);
  }
  template <class... TTypes, class... UTypes>
  bool operator<(const variant<TTypes...>& lhs,
                 const variant<UTypes...>& rhs) {
    using relOpEl_t = internals::relOpElement<variant<TTypes...>, variant<UTypes...>>;
    typedef typename relOpEl_t::Func_t CompFunc_t;
     static const CompFunc_t comp[sizeof...(TTypes)][sizeof...(UTypes)]
        = { (&relOpEl_t::template ImplOne<TTypes>::template ImplTwo<UTypes>::doLess)... };
     if (lhs.empty() && rhs.empty()) return false;
     if (lhs.empty() != rhs.empty()) return false;
     return comp[lhs.index()][rhs.index()](lhs, rhs);
  }
  template <class... TTypes, class... UTypes>
  bool operator>(const variant<TTypes...>& lhs,
                 const variant<UTypes...>& rhs) {
    using relOpEl_t = internals::relOpElement<variant<TTypes...>, variant<UTypes...>>;
    typedef typename relOpEl_t::Func_t CompFunc_t;
     static const CompFunc_t comp[sizeof...(TTypes)][sizeof...(UTypes)]
        = { (&relOpEl_t::template ImplOne<TTypes>::template ImplTwo<UTypes>::doGreater)... };
     if (lhs.empty() && rhs.empty()) return false;
     if (lhs.empty() != rhs.empty()) return false;
     return comp[lhs.index()][rhs.index()](lhs, rhs);
  }
  template <class... TTypes, class... UTypes>
  bool operator<=(const variant<TTypes...>& lhs,
                  const variant<UTypes...>& rhs) {
    return (lhs < rhs || lhs == rhs);
  }
  template <class... TTypes, class... UTypes>
  bool operator>=(const variant<TTypes...>& lhs,
                  const variant<UTypes...>& rhs) {
    return (lhs > rhs || lhs == rhs);
  }
  // 2.?, Specialized algorithms
  template <class... Types>
  void swap(variant<Types...>& x, variant<Types...>& y) {
    using swapEl_t = internals::swapElement<variant<Types...>>;
    typedef typename swapEl_t::Func_t Func_t;
     static constexpr Func_t func[sizeof...(Types)]
        = { (&swapEl_t::template Impl<Types>::doSwap)... };

    if (x.empty() && y.empty()) return;
    size_t idx = -1;
    if (x.empty()) idx = y.index();
    else idx = x.index();
    func[idx](x, y);
  }

    template<class... Types>
    class variant {
    public:
      typedef tuple<Types...> alternatives;

      // 2.? variant construction
      constexpr variant() noexcept: value_type_index(tuple_not_found) {}
      variant(const variant& v) {
        *this = v;
      }
      variant(variant&& v) {
        *this = v;
      }
      template <class T> constexpr explicit variant(const T& t): variant() {
        constexpr size_t alidx = tuple_find<T, tuple<Types...>>::value;
        destructor = internals::store<alidx, Types...>()(mem, t);
        value_type_index = tuple_find<T, tuple<Types...>>::value;
      }
      template <class T> constexpr explicit variant(T&& t): variant() {
        constexpr size_t alidx = tuple_find<T, tuple<Types...>>::value;
        destructor = internals::store<alidx, Types...>()(mem, std::move(t));
        value_type_index = tuple_find<T, tuple<Types...>>::value;
      }

      /*
       template <class T, class... Args>
       constexpr explicit variant(in_place_t, Args&&...);
       template <class T, class U, class... Args>
       constexpr explicit variant(in_place_t,
       initializer_list<U>,
       Args&&...);
       */

      // 2.?, Destructor
      ~variant() { destroy(); }

      /*
       // allocator-extended constructors
       template <class Alloc>
       variant(allocator_arg_t, const Alloc& a);
       template <class Alloc, class T>
       variant(allocator_arg_t, const Alloc& a, T);
       template <class Alloc>
       variant(allocator_arg_t, const Alloc& a, const variant&);
       template <class Alloc>
       variant(allocator_arg_t, const Alloc& a, variant&&);
       */

      // 2.?, `variant` assignment
      variant& operator=(const variant& rhs) {
        typedef typename internals::copyElement<variant>::Func_t CopyFunc_t;
        static const CopyFunc_t copyX[sizeof...(Types)]
          = { (&internals::copyElement<variant>::template Impl<Types>::doCopy)... };
        destroy();
        destructor = copyX[rhs.index()]()(rhs);
        value_type_index = rhs.index();
      }
      variant& operator=(variant&& rhs) {
        typedef typename internals::copyElement<variant>::Func_t CopyFunc_t;
        static const CopyFunc_t moveX[sizeof...(Types)]
          = { (&internals::copyElement<variant>::template Impl<Types>::doMove)... };
        destroy();
        destructor = moveX[rhs.index()]()(rhs);
        value_type_index = rhs.index();
      }

      template <class T> variant& operator=(const T& t) {
        emplace<T>(t);
        return *this;
      }

      template <class T> variant& operator=(T& t) {
        emplace<T>(t);
        return *this;
      }

      template <class T> variant& operator=(T&& t) {
        emplace<T>(std::move(t));
        return *this;
      }

      template <class T, class... Args> void emplace(Args&&...args) {
        clear(); // reset index() in case below throws.
        destructor = internals::store<tuple_find<T, tuple<Types...>>::value,
                                      Types...>()(mem, args...);
        value_type_index = tuple_find<T, tuple<Types...>>::value;
      }
      /*
       template <class T, class U, class... Args>
       void emplace(initializer_list<U>, Args&&...);
       */

      void clear() { destroy(); value_type_index = (size_t)-1; }

      // 2.?, value status
      bool empty() const noexcept { return value_type_index == (size_t)-1; }

      size_t index() const {
        if (empty()) throw bad_variant_access();
        return value_type_index;
      }

      // 2.?, `variant` swap
      void swap(variant& other) {
        fundamentals_vXXXX::swap(*this, other);
      }
      const char* data() const { return mem; }
      char* data() { return mem; }

    private:
      void destroy() {if (!empty()) destructor(&mem[0]);}

      static constexpr size_t max_alternative_sizeof = internals::maxSize<Types...>::value; // exposition only
      char mem[max_alternative_sizeof]; // exposition only
      size_t value_type_index; // (size_t)-1 if empty; exposition only
      std::function<void(void *)> destructor;
    };

} // namespace fundamentals_vXXXX
} // namespace experimental
  // 2.?, Hash support
  template <class T> struct hash;
  template <class... Types>
  struct hash<experimental::variant<Types...>>;
}
