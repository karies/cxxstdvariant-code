#include <memory>
extern "C" int printf(const char*,...);

template <class T>
struct variant {
  variant& operator=(const variant&) { printf("v&\n"); return *this; }
  template <class U>
  variant& operator=(const variant<U>&) { printf("v<U>&\n"); return *this; }
  template <class U>
  variant& operator=(const U&) { printf("U&\n"); return *this; }

  variant& operator=(variant&&) { printf("v&&\n"); return *this; }
  template <class U>
  variant& operator=(variant<U>&&) { printf("v<U>&&\n"); return *this; }
  //template <class U>
  //variant& operator=(U&&) { printf("U&&\n"); return *this; }
};


int main() {
  variant<int> vi, vj;
  variant<float> vf;

  vi = vj;
  vf = vi;
  vf = 12;

  vi = std::move(vj);
  vf = std::move(vi);
  int i = 12;
  vf = std::move(i);

  return 0;
}
