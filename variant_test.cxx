#include "variant.h"
#include <cassert>
#include <stdexcept>

using std::logic_error;

#define HIDE_COMPERRORS 1

struct Lifetime {
  static long fgAlive;
  static long fgConstructed;
  static bool fgDoThrow;
  long fNumber;

  Lifetime(): fNumber(fgConstructed) {
    if (fgDoThrow) throw logic_error("ctor");
    ++fgAlive;
    ++fgConstructed;
  }
  Lifetime(const Lifetime& rhs): fNumber(rhs.fNumber) {
    if (fgDoThrow) throw logic_error("copy ctor");
    ++fgAlive;
    ++fgConstructed;
  }
  Lifetime(Lifetime&& rhs): fNumber(rhs.fNumber) {
    if (fgDoThrow) throw logic_error("move ctor");
    ++fgAlive;
    ++fgConstructed;
  }
  ~Lifetime() {
    // Don't. if (fgDoThrow) throw logic_error("dtor");
    --fgAlive;
  }
  Lifetime& operator=(const Lifetime& rhs) {
    if (fgDoThrow) throw logic_error("op=");
    fNumber = rhs.fNumber;
    return *this;
  }
  Lifetime& operator=(Lifetime&& rhs) {
    if (fgDoThrow) throw logic_error("op= move");
    fNumber = rhs.fNumber;
    return *this;
  }
  static Lifetime make() { return Lifetime(); }
};
bool operator==(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber == rhs.fNumber;
}
bool operator!=(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber != rhs.fNumber;
}
bool operator<(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber < rhs.fNumber;
}
bool operator<=(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber <= rhs.fNumber;
}
bool operator>(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber > rhs.fNumber;
}
bool operator>=(const Lifetime& lhs, const Lifetime& rhs) {
  return lhs.fNumber >= rhs.fNumber;
}

long Lifetime::fgAlive = 0;
long Lifetime::fgConstructed = 0;
bool Lifetime::fgDoThrow = false;


extern "C" int printf(const char*,...);

using namespace std;
using namespace experimental;

void checkTraits(){
  typedef variant<int,double,Lifetime> v_t;
  assert(tuple_size<v_t::alternatives>::value == 2 && "wrong num alternatives");
  assert((tuple_element_t<2, v_t::alternatives>::fgAlive == 0) && "wrong object count");
  v_t v;
  assert(is_alternative<double>(v) && "double really is an alternative");
  assert(!is_alternative<float>(v) && "float should not be an alternative");
  assert((tuple_find<Lifetime, v_t::alternatives>::value == 2) && "wrong idx for Lifetime alternative");
  assert((tuple_find<short, v_t::alternatives>::value == tuple_not_found)
         && "short should not be an alternative");
}

void defConstructed() {
  variant<int, double> v;
  assert(v.empty() && "not empty\n");

  try {
    v.index();
    assert(0 && "missing exception on index()");
  }
  catch (bad_variant_access& e) {
  }

  try {
    get<int>(v);
    assert(0 && "missing exception on get");
  }
  catch (bad_variant_access& e) {
  }
}

void assignInt() {
  variant<int, double> v;
  v = 42;
  assert(!v.empty() && "empty");
  assert(holds_alternative<int>(v) && "holds_alternative is wrong");
  assert(v.index() == 0 && "wrong index");
  assert(get<int>(v) == 42 && "wrong value");
  assert(get<0>(v) == 42 && "wrong value");
  try {
    get<double>(v);
    assert(0 && "missing exception on existing alternative");
  } catch (bad_variant_access& e) {
  }
  try {
    get<Lifetime>(v);
    assert(0 && "missing exception on existing alternative");
  } catch (bad_variant_access& e) {
  }
}

void changeType() {
  variant<int, double> v;
  v = 42;
  v = 12.;
  assert(!v.empty() && "empty");
  assert(holds_alternative<double>(v) && "holds_alternative is wrong");
  assert(v.index() == 1 && "wrong index");
  assert(get<double>(v) == 12. && "wrong value");
  assert(get<1>(v) == 12. && "wrong value");
  try {
    get<int>(v);
    assert(0 && "missing exception on existing alternative");
  } catch (bad_variant_access& e) {
  }
  try {
    get<Lifetime>(v);
    assert(0 && "missing exception on existing alternative");
  } catch (bad_variant_access& e) {
  }
}

void storeNonPod() {
  variant<int,Lifetime> v{Lifetime()};
  assert(holds_alternative<Lifetime>(v) && "holds_alternative is wrong");
  assert(v.index() == 1 && "wrong index");
  assert(get<Lifetime>(v).fNumber == 0 && "wrong value");
  assert(Lifetime::fgAlive == 1 && "wrong count");
  v = 12;
  assert(Lifetime::fgAlive == 0 && "not destructed");
  assert(v.index() == 0 && "wrong type");
  assert(get<0>(v) == 12. && "wrong value");
  assert(get<int>(v) == 12. && "wrong value");
}

void relOps() {
  variant<int,Lifetime> v{Lifetime::make()};
  /*
    // Cannot (yet) compare alternative with variant.
  Lifetime rhs;
  assert(v < rhs && "expected <");
  assert(v <= rhs && "expected <=");
  assert(!(v > rhs) && "expected !>");
  assert(!(v >= rhs) && "expected !>=");
  */

  variant<float, Lifetime> w{Lifetime::make()};
  assert(v < w && "expected <");
  assert(v <= w && "expected <=");
  assert(!(v > w) && "expected !>");
  assert(!(v >= w) && "expected !>=");

#ifndef HIDE_COMPERRORS
  variant<int,double> v1{12.};
  v1 < w;
#endif
}

void swapNonPod() {
  variant<int, Lifetime> a, b;
  a = Lifetime::make();
  b = Lifetime::make();
  auto val = [] (const auto& v) { return get<Lifetime>(v).fNumber; };
  assert(val(b) > val(a) && "unexpected precond");
  swap(a,b);
  assert(val(b) < val(a) && "unexpected swap result");
  a.swap(b);
  assert(val(b) > val(a) && "unexpected swap result");
  a.swap(a);
  assert(val(b) > val(a) && "unexpected self swap result");
}

void exceptSafety() {
  Lifetime::fgDoThrow = true;
  variant<int, Lifetime> a;
  try {
    a = Lifetime();
    assert(0 && "missing throw");
  }
  catch(exception&) {}
  assert(a.empty() && "not empty!");

  Lifetime::fgDoThrow = false;
  a = 12;
  assert(!a.empty() && "should not be empty!");
  assert(a.index() == 0 && "wrong content type!");
  Lifetime obj;
  Lifetime::fgDoThrow = true;
  try {
    a = obj;
    assert(0 && "missing exception");
  }
  catch (exception&) {}
  assert(a.empty() && "not empty!");
}

int main() {
  defConstructed();
  assignInt();
  changeType();
  storeNonPod();
  //relOps();
  //swapNonPod();
  exceptSafety();
}
