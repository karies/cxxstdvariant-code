#include "boost/variant.hpp"
#include <cassert>
using namespace boost;
int main() {
   variant<float, int> v1(12.f);
   variant<float, int> v2(11);

   printf("%d\n", v1 < v2);
   return 0;
}
