#include <iostream>
#include <array>
#include <tuple>
#include <functional>
#include <type_traits>

using namespace std;

template <class... Types>
struct variant {
  typedef tuple<Types...> tuple_t;
  char m_storage[1024];
  size_t m_index;
  size_t index() const { return m_index; }
};

template<size_t I, class Variant>
void set(Variant& var, const typename tuple_element<I, typename Variant::tuple_t>::type& val) {
  memcpy(var.m_storage, &val, sizeof(val));
  var.m_index = I;
}
template<class Variant>
void reset(Variant& var) {
  var.m_index = -1;
}


struct my_visitor {
  template <class Arg>
  ostream& operator()(const Arg& arg) {
    cout << arg;
    return cout;
  }

  ostream& operator()() {
    return cout;
  }
};


template <class Visitor, class Arg>
struct VisitorFwd {
  static decltype(auto) invoke(Visitor& v, const void* arg) {
    return v(*reinterpret_cast<remove_reference_t<Arg>*>(arg));
  }
};

template <class Visitor, class VisitorElse, class Tuple>
struct VisitFuncGen {
  static decltype(auto) invoke_none(VisitorElse& v) {
    return v();
  }

  typedef typename std::result_of<decltype(&VisitFuncGen::invoke_none)(VisitorElse&)>::type result_type;
  template <class Arg> using visitor_fwd_t = VisitorFwd<Visitor, Arg> ;

  template<size_t... I>
  static constexpr decltype(auto) gen(std::index_sequence<I...>) {
    return array<result_type (*)(Visitor&, const void*), sizeof...(I)> {
      (& (visitor_fwd_t<const typename tuple_element<I, Tuple>::type&>::invoke))...
    };
  }
};

template <class Visitor, class... Types>
decltype(auto) visit(Visitor& vis, const variant<Types...>& var) {
  typedef VisitFuncGen<Visitor, Visitor, tuple<Types...>> visit_func_gen_t;

  static constexpr auto fun_tpl
    = visit_func_gen_t::gen(make_index_sequence<sizeof...(Types)>{});
  if (var.index() == -1)
    return vis();
  return fun_tpl[var.m_index](vis, &var.m_storage);
};

template <class Visitor, class VisitorElse, class... Types>
decltype(auto) visit(const Visitor& vis, const VisitorElse& visElse, const variant<Types...>& var) {
  typedef VisitFuncGen<const Visitor, const VisitorElse, tuple<Types...>> visit_func_gen_t;
  static constexpr auto fun_tpl = visit_func_gen_t::gen(make_index_sequence<sizeof...(Types)>{});
  if (var.index() == -1)
    return visElse();
  return fun_tpl[var.m_index](vis, &var.m_storage);
};


int main() {
  variant<int, double, float, char, long, char[2], char[3], char[4], char[5], char[6], char[7]> vid;
  set<1, decltype(vid)>(vid, 42);
  my_visitor vis;

  visit(vis, vid) << endl;
  visit([](auto arg) { cout<<arg; }, [](){ }, vid); cout << endl;

  reset<decltype(vid)>(vid);

  visit(vis, vid) << endl;
  visit([](auto& arg) { cout<<arg; }, [](){ }, vid); cout << endl;
}
