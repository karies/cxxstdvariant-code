#include "variant.h"
#include <cassert>

struct X {
  X() { currentX = this; }
  ~X() { currentX = 0; }
  static X* currentX;
};
X* X::currentX = 0;

struct A: X {};
struct B: X { B() noexcept(false) {} };

void sequencing() {
  // A double-buffered variant not proposed here:
  std::experimental::variant<A,B> v{A()};
  v.emplace<B>(); // copy-constructs B, then destroys A.
  assert(X::currentX && "suprising sequencing!"); // assert fails.
}

